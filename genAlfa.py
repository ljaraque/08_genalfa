# genAlfa.py

import csv
import time
import piface.pfio as pfio 

def authenticate(codeToCheck):
    # use explicit path to let this code run even when not called from its directory (cron, .sh, etc.).
    test_file = '/mnt/arraySlayer03/users/LJ/code/remote_dump/ResidentsCards-20140907-133307.csv'
    csv_file = csv.DictReader(open(test_file, 'rb'), ["id","folio","cardId","status","updated"])
    #print "\nColumn names: "
    #print csv_file.fieldnames
    csv_file.next() # skips headers line

    results=[]
    for row in csv_file:
        id=row["cardId"]
        stat=row["status"] 
        #print "\n"+id
        if id == codeToCheck and stat=="1":
            #print "Found!"
            results.append("Found")
        else:
            #print "Not Found!"
            results.append("Not Found")

    #print "\nmatches: "
    if results.count("Found")>=1:
        return(1)
    else:
        return(0)
    #print "\nresults:"
    #print results

def open_car():
    
    pfio.init()
    #print("Abriendo Puerta Peatonal")
    #print("Abro Puerta")
    pfio.digital_write(1, 1)
    time.sleep(0.2)
    pfio.digital_write(1, 0)
    #print("Abierto")
    return(0)


def open_pedestrian():

    pfio.init()
    #print("Abriendo Puerta Peatonal")
    #print("Abro Puerta")
    pfio.digital_write(0, 1)
    time.sleep(0.2)
    pfio.digital_write(0, 0)
    #print("Abierto")
    return(0)

def main():
    pass

if __name__ == '__main__':
        main()

