### file `genAlfa.py`   

This code is part of Xalfa Smart Community Solution.   

* **Description:** This python code is implement the authentication of remote requirements from multiple uGen for activation of actuators (e.g. door opening). This code authenticates via checking `.csv` file and activates actuators.   
* **author:** ljaraque   

**Highlights:**   

1. There are 3 functions implemented: `authenticate(codeTocheck)`, `open_car()` and `open_pedestrian()`.   
2. `authentication()` function returns 1 if the code is found and with status =1, in the `.csv` database of `cardIDs` and 0 if not.   
3. `open_car()` and `open_pedestrian()` functions open car or pedestrian doors, respectively.   

